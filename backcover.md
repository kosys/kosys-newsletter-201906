---
title: 裏表紙
author: 安坂悠
sectionClass: backcover
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---
<img src="./images/backcover.png" class="img-fullpage"  />
