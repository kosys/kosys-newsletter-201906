# ご挨拶

2018年も当団体の活動に関し、格別のご支援ご協力を賜り、厚くお礼申し上げます。

創作活動の中でも、特に手描きアニメの制作は、時間、労力、費用を要します。しかし、皆様のご協力により、「こうしす！」第3話Part2も2019年内にも完成し公開できる見通しとなりました。皆様に深く感謝いたします。

さて、昨今の動向として特筆すべき点は「こうしす！」の追っ手が現れてきているという点でしょう。物語を通じたセキュリティ普及啓発手法に注目が集まりはじめ、セキュリティをテーマとしたコンテストも開催されるに至っています。ハッカーやクラッカーなどセキュリティをテーマとした作品が増えています。さらに、度重なる大規模災害や、2020年に迫る東京オリンピックの開催に伴い、官民問わずセキュリティ向上への機運が高まりを肌に感じる一年でした。

とはいえ、企業や組織内のセキュリティをテーマとしたコメディアニメ作品という分野では、「こうしす！」が先行していると考えています。この強みを最大限に活かし、こうしす！に関わるすべての人が活躍の場を広げられることをテーマに、尽力して参りたいと考えております。

2019年も当団体及び「こうしす！」を何卒よろしくお願いいたします。


<div style="text-align: right;">
2019年6月1日<br />
Open Process Animation Project Japan<br />
代表理事 井二かける
</div>