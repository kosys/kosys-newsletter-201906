---
balanceSheetData: [
    {"accountName": "Ⅰ 資産の部", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
    {"accountName": "1.流動資産", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
    {"accountName": "現金預金", "balance1": 164393, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "未収金", "balance1": 998, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "流動資産合計", "balance1": null, "balance2": 165391, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
    {"accountName": "2.固定資産", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
    {"accountName": "固定資産合計", "balance1": null, "balance2": 0, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
    {"accountName": "資産合計", "balance1": null, "balance2": null, "balance3": 165391, "cssClass": "item-depth-1 item-sum item-break-2"},
    {"accountName": "Ⅱ 負債の部", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header item-break-3"},
    {"accountName": "1.流動負債", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1"},
    {"accountName": "未払金", "balance1": 147664, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "預り金", "balance1": 15620, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "役員借入金", "balance1": 54000, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "流動負債合計", "balance1": null, "balance2": 217284, "balance3": null, "cssClass": "item-depth-2 item-sum item-group-header item-break-1"},
    {"accountName": "2.固定負債", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1"},
    {"accountName": "固定負債合計", "balance1": null, "balance2": 0, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
    {"accountName": "負債合計", "balance1": null, "balance2": null, "balance3": 217284, "cssClass": "item-depth-1 item-sum item-break-2"},
    {"accountName": "Ⅲ 正味財産の部", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
    {"accountName": "前期繰越正味財産", "balance1": null, "balance2": -22625, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "当期正味財産増減額", "balance1": null, "balance2": -29268, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "正味財産合計", "balance1": null, "balance2": null, "balance3": -51893, "cssClass": "item-depth-1 item-sum item-break-2"},
    {"accountName": "負債及び正味財産合計", "balance1": null, "balance2": null, "balance3": 165391, "cssClass": "item-depth-1 item-sum item-break-3"},
]
profitAndLossData: [
    {"accountName": "I 経常収益", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
    {"accountName": "1.受取会費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
    {"accountName": "正会員受取会費", "balance1": 5000, "balance2": null, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "賛助会員受取会費", "balance1": 46029, "balance2": 51029, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "2.受取寄附金", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header item-break-1"},
    {"accountName": "受取寄附金", "balance1": 583540, "balance2": 583540, "balance3": null, "cssClass": "item-depth-2"},
    {"accountName": "経常収益計", "balance1": null, "balance2": null, "balance3": 634569, "cssClass": "item-depth-1 item-sum item-break-1 item-break-2"},
    {"accountName": "II 経常費用", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-0 item-group-header"},
    {"accountName": "1.事業費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
    {"accountName": "（1）人件費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header"},
    {"accountName": "人件費計", "balance1": 0, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
    {"accountName": "（2）その他経費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header item-break-1"},
    {"accountName": "業務委託費", "balance1": 401760, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "旅費交通費", "balance1": 4230, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "通信運搬費", "balance1": 11575, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "消耗品費", "balance1": 49490, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "研修費", "balance1": 20000, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "支払手数料", "balance1": 59883, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "広告宣伝費", "balance1": 6928, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "その他経費計", "balance1": 553866, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
    {"accountName": "事業費計", "balance1": null, "balance2": 553866, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
    {"accountName": "2.管理費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-1 item-group-header"},
    {"accountName": "（1）人件費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header"},
    {"accountName": "人件費計", "balance1": 0, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
    {"accountName": "（2）その他経費", "balance1": null, "balance2": null, "balance3": null, "cssClass": "item-depth-2 item-group-header item-break-1"},
    {"accountName": "通信運搬費", "balance1": 80383, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "消耗品費", "balance1": 2930, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "支払手数料", "balance1": 26658, "balance2": null, "balance3": null, "cssClass": "item-depth-3"},
    {"accountName": "その他経費計", "balance1": 109971, "balance2": null, "balance3": null, "cssClass": "item-depth-3 item-sum item-break-1"},
    {"accountName": "管理費計", "balance1": null, "balance2": 109971, "balance3": null, "cssClass": "item-depth-2 item-sum item-break-1"},
    {"accountName": "経常費用計", "balance1": null, "balance2": null, "balance3": 663837, "cssClass": "item-depth-1 item-sum item-break-2"},
    {"accountName": "当期正味財産増減額", "balance1": null, "balance2": null, "balance3": -29268, "cssClass": "item-depth-2 item-sum item-group-header item-break-3"},
    {"accountName": "前期繰越正味財産額", "balance1": null, "balance2": null, "balance3": -22625, "cssClass": "item-depth-2 item-sum"},
    {"accountName": "次期繰越正味財産額", "balance1": null, "balance2": null, "balance3": -51893, "cssClass": "item-depth-2 item-sum item-break-3"}
]
---
# 2018年度決算報告

## 期間
2018年1月1日から2018年12月31日まで

## 活動計算書
<div class="unit">
    単位：円
</div>
<FinancialStatementsTable :sheetData="$page.frontmatter.profitAndLossData" />

## 貸借対照表
<div class="unit">
    単位：円
</div>
<FinancialStatementsTable :sheetData="$page.frontmatter.balanceSheetData" />


## 財務諸表の注記

### 重要な会計方針

財務諸表の作成は、NPO法人会計基準（2010年7月20日　2017年12月12日最終改正　NPO法人会計基準 協議会）によっています。ただし、財産目録の作成は省略しております。

### 施設の提供等の物的サービスの受入の内訳

当期は計上しておりません。

### 活動の原価の算定にあたって必要なボランティアによる役務の提供の内訳

当期は計上しておりません。

### 借入金の増減内訳
<div class="unit">
    単位：円
</div>
<table class="financialSheet">
    <tr>
        <th>科目</th>
        <th>当期残高</th>
        <th>当期借入</th>
        <th>当期返済</th>
        <th>期末残高</th>
    </tr>
    <tr>
        <td>役員借入金</td>
        <td>54,000</td>
        <td>54,000</td>
        <td>54,000</td>
        <td>54,000</td>
    </tr>
    <tr>
        <td>計</td>
        <td>54,000</td>
        <td>54,000</td>
        <td>54,000</td>
        <td>54,000</td>
    </tr>
</table>


### 役員及びその近親者との取引の内容

役員及びその近親者との取引は以下の通りです。

#### 活動計算書

<div class="unit">
    単位：円
</div>
<table class="financialSheet">
    <tr>
        <th>科目</th>
        <th>計算書類に計上された金額</th>
        <th>内、役員との取引</th>
        <th>内、近親者及び支配法人等との取引</th>
    </tr>
    <tr>
        <td>正会員受取会費</td>
        <td>5,000</td>
        <td>5,000</td>
        <td>0</td>
    </tr>
    <tr>
        <td>受取寄附金</td>
        <td>583,540</td>
        <td>10,000</td>
        <td>0</td>
    </tr>
    <tr>
        <td>計</td>
        <td>588,540</td>
        <td>15,000</td>
        <td>0</td>
    </tr>
</table>

#### 貸借対照表

<div class="unit">
    単位：円
</div>
<table class="financialSheet">
    <tr>
        <th>科目</th>
        <th>計算書類に計上された金額</th>
        <th>内、役員との取引</th>
        <th>内、近親者及び支配法人等との取引</th>
    </tr>
    <tr>
        <td>役員借入金</td>
        <td>54,000</td>
        <td>54,000</td>
        <td>0</td>
    </tr>
    <tr>
        <td>計</td>
        <td>54,000</td>
        <td>54,000</td>
        <td>0</td>
    </tr>
</table>


<style lang="stylus">
.unit {
  text-align: right ;
}

.financialSheet{
  width: 100%;
  table-layout: fixed;
  border: solid 1px black;
  display: table;
  margin: 0;

  * {
    background: none;
  }

  tr {
    border: none;
  }
  th {
    border: solid 1px black;
  }
  
  td {
    border: solid 1px black;
    border-top: none;
    border-bottom: none;
    padding: 5px;
  }

  .item-depth-0 {
    .accountName {  }
  }
  .item-depth-1 {
    .accountName { padding-left: 2em; }
  }
  .item-depth-2 {
    .accountName { padding-left: 4em; }
  }
  .item-depth-3 {
    .accountName { padding-left: 6em; }
  }

  .balance {
    text-align: right;
  }

  .item-sum {
    .balance, .accountName { font-weight: bold; }
  }

  .item-group-header .accountName { font-weight: bold; }
  
  .item-break-1 .balance-1 { border-top: solid 1px black; }
  .item-break-2 .balance-2 { border-top: solid 1px black; }
  .item-break-3 .balance-3 { border-top: solid 1px black; }
  
}

.col-accountName { width: auto; }
.col-balance { width: 8em; }


.print .financialSheet  { 
    font-size: 8pt;
        
    .col-accountName { width: auto; }
    .col-balance { width: 6em; }
}
</style>

