---
title: 寄稿イラスト
author: るみあ
sectionClass: illust-section
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---
<img src="./images/rumia.png" class="img-fullpage"  />

<section class="poem">
<h2>「梅雨明け」</h2>
少し未来のこと。<br />
CISOに就任したアカネと、秘書の芽依。<br />
突然の雨に見舞われた二人。<br />
アカネは自らの肩を濡らしながらも、<br />
芽依に傘を差し出す。<br />
やがて雨が上がり、陽が差した。<br />
そんな一瞬。
</section>

<AuthorProfiles>
<AuthorProfileItem authorTitle="画" authorName="るみあ">
</AuthorProfileItem>
<AuthorProfileItem authorTitle="文" authorName="井二かける">
</AuthorProfileItem>
</AuthorProfiles>