---
title: 表表紙
author: 安坂悠
sectionClass: frontcover
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---
<img src="./images/frontcover.png" class="img-fullpage"  />