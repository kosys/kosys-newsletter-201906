---
author: ０たか
---
# 【必見】 こうしす！見どころ紹介

  　こんにちは、０たかです。3話から作画や字幕などでちょこちょこと参加しています。  
  　今回は見どころ紹介と題して「こうしす！」1話～3話part1を振り返りながら、お気に入りの場面や補足して説明したい場面を紹介して、あれやこれやと解説していきたいと思います。紹介したいカットはたくさんありましたが、なるべく多くコメントできそうなカットを優先してピックアップしました。より一層「こうしす！」を楽しめるきっかけになれば幸いです。  

## こうしす！見どころ
### ◆ 第1話  
  　第1話は **Windows XPのサポート切れ** を題材に、サポートが終了してもOSをアップグレードせず使い続けていた場合にどんなことが起きるかが鮮やかに表現されています。コミカルにテンポよく物語が進行していくので見ていて爽快感があります。  
  　そういえば、来年(2020年)1月にはWindows 7のサポートも終了します。7を使い続けている場合は、サポート期限終了後に攻撃者の標的/踏み台にされないように、早めに対策を取りましょうね！

#### ● <u>S03C15 [05:45]</u>
  <img width="350" alt="EP01S03C15" src="./images/EP01S03C15.png"><br>  
    ［ キャラ作画：なめたけさん, ホワイトボード作画：Butameronさん ］
  >　宏佳の首の傾き、お気づきの方も多いかと思いますが、これは **シャフ度(シャフト角度)** と呼ばれるものです。シャフトというアニメ会社の作品に多い演出なのですが、こんなところでもお目にかかれるとは。宏佳のクールな性格・厳しい表情と相まってとてもしっくり来ますね。  
  　後ろのホワイトボードにも着目です。
  インシデント対応の経過とともに、京姫鉄道のネットワーク構成が一望できるようになっています。
  経時的に書き込みが変化しているので、それを順を追って見ていくと、8F広報部(の芽依のPC)から徐々にウイルスの感染が拡大し、さらなる感染拡大防止のためにネットワークを逐次切断・隔離しているんだな、とウイルス感染と対応の進行の様子がよく分かると思います。  
  　ネットワーク構成を見ると、STP(スパニングツリープロトコル)やOpenFlowなどの技術が使われていてなかなか興味深いところでもあります。

#### ● <u>S03C20 [06:25]</u>
  <img width="350" alt="EP01S03C20" src="./images/EP01S03C20.png"><br>  
    ［ 作画：Butameronさん ］
  >　ネタが大量に詰め込まれたカットです。  
  　まずは電光掲示板から見ましょうか。ハッカー(クラッカー)のいたずらにより、内容がめちゃくちゃにされていますね。いいな、999に乗ってアンドロメダに行ってみたいです。
  「喜多方ラーメン」という表記は、駅の電光掲示板に「喜多方ラーメン」と誤って表示される事件があったようで、その事件を反映して改良？したものなんだとか。
  喜多方ラーメンの事を考えていたら腹モスクワ、なんちゃって。（モスクワだけに寒くてすみませんw）  
  　ニッコ生の画面にもいろいろとネタが仕込まれていますね。来場数とコメント数が一昔前のディスプレイ解像度(1024x768)だったり、コメントにある"よしみちゅ先生"が3話で登場したり、今まさにコメントを書き込もうとしていたり(少佐？)・・・。
  まだまだいろいろありそうです。

#### ● <u>S04C08 [10:31]</u>
  <img width="350" alt="EP01S04C08" src="./images/EP01S04C08.png"><br>  
    ［ 作画：はるまきごはんさん ］
  >　アカネがインターネットへの接続を物理的に切断した場面です。してやったりの笑顔のアカネと目をぐるぐると回してうなだれるCIOの対比がコミカルで面白いです。  
  　なお、今回は突破される寸前だったため緊急回避策としてネットワーク切断をしていますが、対応としてまずネットワークを切断することが最善でない場合もあります。時には、訴訟などに備えて攻撃の証拠となるデータを収集・保管すること( **フォレンジック** )を優先して行うこともあるようです。これは、マルウェアの中には、ネットワークが切断されたことを検知して証拠隠滅を図るタイプのものもあるからです。緊急性やリスクの大きさを考慮してどちらを優先するのか、総合的に対応を決断していく必要があります。(難しい)

### ◆ 第2話  
  　さて、第2話はシステム開発現場の闇がリアルに描かれている回となっています。エンジニアの方々が見るとどうやら胃が痛くなるようです。無駄に胃を傷めないようにエンジニアの方は牛乳を飲んでから現実から5ｍ以上離れて見るようにしましょうw  
  　情報セキュリティの観点からは **SQLインジェクション** が取り上げられています（詳しくは後述）。線の細かさや動きの滑らかさなど作画レベルが1話と比べ大きく向上していて、しかも長さが25分程度あり、自主制作とは思えないクオリティに驚いたものでした。

#### ● <u>S01C02 [00:17]</u>
  <img width="350" alt="EP02S01C02" src="./images/EP02S01C02.png"><br>  
    ［ キャラ作画：旭洋さん, 背景作画：Butameronさん ］
  >　京姫鉄道福住駅です。1話の最初にも同じ場所の描写がありますが、より写実的で精緻な背景になっています。  
  　チカチカと電車の窓に反射する太陽光が眩しく、リアルさを際立てています。さらに、そこから水平方向へ青色光のレンズフレアが伸びていて美しいです。  
  　ホームのコンクリート上のあたりにもやもやとする効果が加えられています。空気の密度の違いで起こる現象である **陽炎（かげろう）** がうまく表現されていて、見ているこちらも暑い気分になってしまいます。

#### ● <u>S01C13 [00:59]</u>
  <img width="350" alt="EP02S01C13" src="./images/EP02S01C13.png"><br>  
    ［ キャラ作画：旭洋さん, 夏野未来さん, 背景作画：Butameronさん ］
  >　台風による風雨が強く、國鉄路線は運転を見合わせている中、京姫鉄道は運転再開をしています。  
  　画面右上の表記を見ると「クモハK133-21」とあり、K133系（京姫鉄道の直流近郊形電車らしい）の電車で、モーターがある車両であることがわかります。画面の右端を見ると女の子の目線がアカネの方に向いていて微笑ましいです。この女の子こそがのちにアカネの部下となる網干ちゃん(当時中学3年生)なのですが（2話の最後の方で再び登場します）、この頃にはもう京姫鉄道への就職を意識していたのかな。  
  　そういえば、このカットのアカネのアナウンスは実際の車両でアフレコが行われたとか(こだわりがスゴイ)。。。気になる方は、 [こうしす！メイキング動画](https://www.nicovideo.jp/watch/1467538517) をチェックしてみてね☆

#### ● <u>S04C13 [05:19]</u>
  <img width="350" alt="EP02S04C13" src="./images/EP02S04C13.png"><br>  
    ［ キャラ作画：もぐもぐさん, 背景作画：Butameronさん ］
  >　葛城の手と顔がびろーんと伸びています。このように長く伸びた軌道を描いたり、動いているものの破片を描いたりして、残像を表現することをアニメーション用語で **「オバケ」表現** と呼ぶようです。このような餅のようにびろーんと伸びている形の「オバケ」を見かけたのは初めてで新鮮でしたが、動いているのを見るとしっかりと動きに迫力がついていて、表現として面白いと思いました。中割りとはいえキャラを崩すのは勇気がいりますが（うまく崩さないと作画崩壊だと言われそうなのでw）、そのうち挑戦してみたいところです。

#### ● <u>S04C21 [05:54]</u>
  <img width="350" alt="EP02S04C21" src="./images/EP02S04C21.png"><br>  
    ［ キャラ作画：安坂悠さん, 背景作画：もぐもぐさん ］
  >　「そもそも　うがあああああああ」と葛城がブチギレるシーンです。  
  　このカットをよく見ると、後屈して叫ぶ前にまず一瞬少し前屈してぐっと息を溜めています。ある方向への動作をする前に一度それと逆方向への動きをつけ（ジャンプする直前に一瞬しゃがむような動作）、見る人に次に何が起きるかを予測させることを **「アンティシペーション」** といいます。一瞬前屈する動作がまさにそれになっていて、動作の力強さが際立っています。

#### ● <u>S04C28 [06:38]</u>
  <img width="350" alt="EP02S04C28" src="./images/EP02S04C28.png"><br>  
    ［ キャラ作画：Lingerieさん, 背景作画：Butameronさん ］
  >　某有名絵画のパロディですね。とてもリアルなタッチでキャラが描かれています。
  特に葛城の顔がリアルで、こんな感じの人が本当に存在しそうと思ってしまいます。
  光が左の手前側から単方向的に当たっていることで、明るい部分と暗い部分の差が大きくなっていて、迫力がありますね。  
  　この場面はキノコワークフロー vs タケノコグループウェアというきのこたけのこ戦争でたけのこ派が大勝利しているところですが、皆さんはきのことたけのこどちらが好きですか？私はどちらも美味しくて好きですが、どちらかというとかずのこかな。

#### ● <u>S90C22 [10:38]</u>
  <img width="350" alt="EP02S90C22" src="./images/EP02S90C22.png"><br>  
    ［ 実写：Butameronさん ］
  >　**SQLインジェクション** や **XSS(クロスサイトスクリプティング)** といったインジェクション攻撃についての説明されている場面です。台本の空欄を使った説明は非常にわかりやすいです。  
  　原理を理解した結菜が 「カギ括弧の記入を許している時点でアホなだけでしょ。」 と言っていますがまさにその通りで、不正コードが埋め込まれないように工夫を凝らさなければなりません。一般的には、下記などのアイデアがあります。  
    <ol><li>括弧のような特殊な文字や不正な構文を検知してエラーを返す</li>
    <li>括弧のような特殊な文字を他の文字列に置き換えて処理する</li> 
    <li>ここは後で自由に書き込めるところ、としておいて他の部分の構文解析を先に済ませてしまう（アニメの例でいうと、ここのセリフはアフレコ段階で自由に入れてもらう、としておいて先にコンテや原画を作成するイメージです。こうすれば結菜が使い走りされるカットは原理的に生じ得ません。）</li></ol>
  　アカネがSQLインジェクション攻撃の脆弱性への対応として、DbParameter を使うように言っていますが、それはこれを使えば3.を実現できるからです。

#### ● <u>S06C01 [14:57]</u>
  <img width="350" alt="EP02S06C01" src="./images/EP02S06C01.png"><br>  
    ［ キャラ作画：るみあさん, 背景作画：Butameronさん ］
  >　キャラデザなどでおなじみのるみあさん作画です。寝ている結菜を見つめるアカネの愛情あふれる表情がたまりませんね。（この次のカットS06C02の表情も良いです）  
  　基本他人に無関心なアカネですが、なんやかんや言って面倒見が良い一面もあったりします。まあそんな一面があるから、芽依に好かれてしまうのですが…それは別の話。そのあたりが気になる方には、[こうしす！ボイスドラマ「入社式編」](https://www.nicovideo.jp/watch/1427901435)をおすすめしておきましょう。

#### ● <u>S11C03 [22:04]</u>
  <img width="350" alt="EP02S11C03" src="./images/EP02S11C03.png"><br>  
    ［ キャラ作画：もんじゅさん, 背景作画：夏野未来さん ］
  >　Windows Server 2012 R2 サポート期限から1年が経過した未来の世界(2024年?)。  
  　アカネが左目につけている機器はなんでしょうか。脚本によると「未来風のスカウターっぽいヘッドマウントディスプレイ」とのことですが、便利そう。ほしい！

### ◆ 第3話 part 1  
  　第3話はミステリー仕立てです。一体何が起きているのか、犯人は誰なのかを考えながら見ると面白いと思います。  
  　ふむふむ、社内からよしみちゅ先生の自宅サーバー宛に **DoS攻撃** らしき通信があるとのことです。キーボードのScroll Lockランプが誰もいないのに点灯したのが不気味です。一体どうなっているんでしょう？ part2 以降をお楽しみに。  
  　それと、第3話では組織での **情報セキュリティマネジメント** が大きなテーマとなっているので、そのあたりにも注目してみてくださいね！

#### ● <u>S00C01 [00:00]</u>
  <img width="350" alt="EP03S00C01" src="./images/EP03S00C01.png"><br>  
    ［ 背景作画：Stars/Doverさん, 桜吹雪の特殊効果：クニキチさん ］
  >　桜が舞い散る春の田園を電車が駆け抜けるという、趣深いカットです。  
  　背景は、近くにある桜の木や田んぼは彩度が高く、遠くの方にいくにつれて色相が青みがかり彩度が低く霞んだようになっています。こういうのを **空気遠近法** といい、画面の奥行きを表現することができます。山がずっと遠くにあるように見え、山でも裾野より頂上のほうが遠くに見えるのは、この空気遠近法がうまく使われているからだと思います。  
  　桜の花が舞い散るエフェクトも美しく、いつまでもループ再生したくなります。

#### ● <u>S02C06 [04:09]</u>
  <img width="350" alt="EP03S02C06" src="./images/EP03S02C06.png"><br>  
    ［ キャラ作画：０峻, 背景：井二かけるさん, 廣田智亮さん ］
  >　私が作画を担当したカットです。「ウイルスを除去」してくれた敬川(全身タイツの人)に2人でお礼を言った後、余部が芽依に対し「他人に迷惑をかけないようにしなさい💢」と叱る場面です。  
  　このカットはズームアップしてもジャギー(低解像度のものを拡大した時に出るあのギザギザのこと)が目立たないようにするために、解像度を大きくした大判カットとして作成されています。  
  　キャラの性格が表情に出るように心がけたので、芽依のなんとなくあどけないところ、余部の非常に外聞を気にするところ、敬川の何かたくらんでいそうなところがキャラの表情などから感じ取ってもらえれば嬉しいです。
  また、キャストの方々には絵に合うようにうまく声をのせてくださり感謝しています。ありがたや～。  

#### ● <u>S02C12 [04:58]</u>
  <img width="350" alt="EP03S02C12" src="./images/EP03S02C12.png"><br>  
    ［ 作画：如月ほのかさん ］
  >　デフォルメされた芽依ちゃんが左右に揺れるのがかわいいです 📞 ( ○ △ ○ ) ※  
  　確かに「DoS攻撃」と「どすこい」、発音が似てますね。  
  　**DoS攻撃** の本質は、DoSが **Denial of Service** の略であるように、サービスを機能不全に陥らせることです。DoS攻撃とはどんなものか、イメージしやすいように身近な例で例えると、
    <ul>
    <li>レストランで、調理が大変そうなメニューを大量に注文してスタッフを忙殺させ、他の客が注文したメニューを作れなくする</li>
    <li>コミケで、特定のサークルの同人誌をすべて買い占めてしまい、他の人が購入できないようにする</li>
    <li>コールセンターへの電話で、必要以上に長時間オペレーターを拘束し、他の人がつながらないようにする</li>
    <li>学校で、教師をわざと怒らせて授業を中断させ、他の生徒が授業を受けられなくする</li></ul>
  といったようなものがあります。アイデアは様々ですが、いずれも他の人がサービスを利用できない状態にしているというのがポイントです。分かったかな?、芽依ちゃん。（伝わってなさそう）  
  　ちなみに、よしみちゅ先生のブログ（ S02C02 [03:43] ）も参照すると理解が深まると思います。

## 著者紹介

<AuthorProfiles>
<AuthorProfileItem authorName="０たか">
Twitter: https://twitter.com/0takaX
</AuthorProfileItem>
</AutorProfiles>