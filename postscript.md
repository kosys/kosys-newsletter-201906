# 編集後記

OPAP-JP発足後、会報の発行は今回が初めてとなります。

サクッと作って入稿すれば良いだけという甘い見込みで始めた事でしたが、とても大変でした。ちょっと作るだけでもやらなければならないことが山のようにあり、毎回コミケなどのイベントに同人誌を出展されている皆様のすごさが分かったような気がします。

今回は、この会報の作成のため、VuePress、Vivliostyle、viola-savepdf、press-readyなど、様々なFLOSSを使用しました。紙面の都合で、すべてを紹介することはできませんが、これらの有用なソフトウェアを無償で提供くださった皆様に心より御礼申し上げます。

また、事前情報の通り、様々な罠に無事はまりましたが、その都度、適切なアドバイスをくださった皆様に感謝いたします。

最後になりましたが、こうしす！の制作にご参加くださった皆様、クラウドファンディングを通じてご支援くださった皆様、SNS上でご意見やアドバイスをくださった皆様、こうしす！の宣伝にご協力くださった皆様、そして、本冊子の作成にご参加・ご協力くださった皆様に心より御礼申し上げます。

そんなわけで、次回の発行を行えるかはわかりませんが、仮に発行するとしても、次回はもう少しボリューム少なめになると思います。それほど期待せずにお待ちいただければ幸いです。

最後までご覧くださりありがとうございました。  
今後ともOPAP-JP及び「こうしす！」を何卒よろしくお願いいたします。

<AuthorProfiles>
<AuthorProfileItem authorTitle="編集" authorName="井二かける">
</AuthorProfileItem>
</AuthorProfiles>

<div class="break-after"></div>

## おまけ: 本編未登場キャラクター紹介

<img src="./images/tsubame.png" style="max-width: 70%; float: left;" />

<h3 style="text-align: center;">ツバメ</h3>
<div class="small">
<p>24世紀の人工生命体。地球共和国技術復興省考古調査局歴史研究室電子データ調査課 研究員。過去の世界から物体を複製する時空複製器を用いて、滅亡した人類の歴史を調査する日々を送る。とても元気な猪突猛進タイプで、好奇心が暴走した結果、祝園アカネを未来の世界に複製してしまう。</p>
<p class="break-word">フルネームは「Cubame.{1a343a4f7abd4d326c8ae7929b605eda2ee4727e061e7024d5d1d950b9b3a00d}」</p>
</div>

<div style="clear: both;"></div>
<div class="donate">
<strong>― 寄附歓迎！―</strong> <br />
当団体へのご支援は主に以下のウェブサイトにて受け付けております。 <br />
(※2019年6月現在の情報です)
<table>
    <tr>
        <td>
        <img src="./images/qrcode_syncable.png" />
Syncable - ご寄附<br />
<a href="https://syncable.biz/associate/opap-jp/donate/">https://syncable.biz/associate/opap-jp/donate/</a>
        </td>
        <td>
            <img src="./images/2dcode_pixiv.png" />
    PIXIV FANBOX - 賛助会員<br />
    <a href="https://www.pixiv.net/fanbox/creator/6788743">https://www.pixiv.net/fanbox/creator/6788743</a>
        </td>
    </tr>
</table>
</div>