---
sectionClass: colophon-section
overrideStyles: "
@page {
}
@page:left {
    @top-left {
        content: '';
        border-bottom: none;
    }
}

@page:right {
    @top-right {
        content: '';
        border-bottom: none;
    }
}"
---
# 奥付

<h2 class="book-title">こうしす！NEWSLETTER 2019年6月号</h2>
<table class="print-history">
    <tr>
        <td class="date">2019年6月10日</td>
        <td class="description">初版第1刷　発行</td>
    </tr>
</table>
<table class="book-credit">
    <tr>
        <th>発行者</th>
        <td>
            オープンプロセスアニメプロジェクトジャパン<br />
            <a href="https://opap.jp/">https://opap.jp/</a>
        </td>
    </tr>
    <tr>
        <th>表紙デザイン</th>
        <td>安坂　悠</td>
    </tr>
    <tr>
        <th>イラスト</th>
        <td>
            るみあ
        </td>
    </tr>
    <tr>
        <th>執筆協力</th>
        <td>
            ０たか、井二かける、玉虫型偵察器、森野 憂希
        </td>
    </tr>
    <tr>
        <th>編集</th>
        <td>井二かける</td>
    </tr>
    <tr>
        <th>素材提供</th>
        <td>
            &copy;OPAP-JP contributors (<a href="https://opap.jp/contributors">https://opap.jp/contributors</a>) <br />
            クリエイティブ・コモンズ 表示 4.0 国際ライセンスの許諾に基づき、一部を改変して掲載
        </td>
    </tr>
    <tr>
        <th>著作権表記</th>
        <td>
            &copy;OPAP-JP contributors (<a href="https://opap.jp/contributors">https://opap.jp/contributors</a>) <br />
            特に記載のない限り、クリエイティブ・コモンズ 表示 4.0 国際ライセンス（<a href="https://creativecommons.org/licenses/by/4.0/deed.ja">https://creativecommons.org/licenses/by/4.0/deed.ja</a>）のもとに利用を許諾。
        </td>
    </tr>
    <tr>
        <th>印刷所</th>
        <td>
            株式会社ブックフロント<br />
            製本直送.com <br />
            <a href="https://www.seichoku.com/">https://www.seichoku.com/</a><br />
            サイズ: A5 (塗り足しあり)<br />
            表紙: カラー＆ラミネート加工（マットコート）<br />
            本文: カラー、ホワイトしらおい、無線綴じ<br />            
        </td>
    </tr>
</table>

<div class="small">
Printed in Japan.<br />
<br />
乱丁、落丁はご容赦ください。<br />
<br />
この印刷物は非売品です。当団体の会報誌として、会員及び当団体に特別の関係を有する者に対して無償頒布するために印刷されたものです。この印刷物そのものを有償で譲渡、販売、転売等をすることはご遠慮ください。なお、この規定は本冊子の内容の利用を制限するものではありません。
</div>
