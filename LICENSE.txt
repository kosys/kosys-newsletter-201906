Copyright (C) OPAP-JP contributors.
https://opap.jp/contributors

Source code: MIT 
Contents: CC-BY 4.0 https://creativecommons.org/licenses/by/4.0/deed.ja



Bundled Fonts
--------------
Google Fonts
https://fonts.google.com/attribution


Noto Serif JP	SIL Open Font License, 1.1

NotoSerifJP-ExtraLight.otf: Copyright 2017 Adobe Systems Incorporated (http://www.adobe.com/).
NotoSerifJP-Light.otf: Copyright 2017 Adobe Systems Incorporated (http://www.adobe.com/).
NotoSerifJP-Regular.otf: Copyright 2017 Adobe Systems Incorporated (http://www.adobe.com/).
NotoSerifJP-Medium.otf: Copyright 2017 Adobe Systems Incorporated (http://www.adobe.com/).
NotoSerifJP-SemiBold.otf: Copyright 2017 Adobe Systems Incorporated (http://www.adobe.com/).
NotoSerifJP-Bold.otf: Copyright 2017 Adobe Systems Incorporated (http://www.adobe.com/).
NotoSerifJP-Black.otf: Copyright 2017 Adobe Systems Incorporated (http://www.adobe.com/).


Noto Sans JP	SIL Open Font License, 1.1

NotoSansJP-Thin.otf: Copyright 2012 Google Inc. All Rights Reserved.
NotoSansJP-Light.otf: Copyright 2012 Google Inc. All Rights Reserved.
NotoSansJP-Regular.otf: Copyright 2012 Google Inc. All Rights Reserved.
NotoSansJP-Medium.otf: Copyright 2012 Google Inc. All Rights Reserved.
NotoSansJP-Bold.otf: Copyright 2012 Google Inc. All Rights Reserved.
NotoSansJP-Black.otf: Copyright 2012 Google Inc. All Rights Reserved.

Noto Emoji	    SIL Open Font License, 1.1

NotoEmoji-Regular.ttf: Copyright 2013 Google Inc. All Rights Reserved.
